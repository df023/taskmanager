<?php

namespace App\Controllers;

use Core\View;
use App\Models\Task;
use App\Helpers\Image;

class Tasks extends \Core\Controller
{
  use \App\Helpers\FormValidate;

  private $taskDraft = null;

  public function index()
  {
    $sort = isset($_GET['sort']) ? $_GET['sort'] : 'created_at';

    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $perPage = 3;
    $start = $page > 1 ? $page * $perPage - $perPage : 0;

    list($total, $tasks) = Task::fetch($start, $perPage, $sort);
    $pages = ceil($total / $perPage);

    View::render('Tasks/index', [
      'tasks' => $tasks,
      'pages' => $pages,
      'currentPage' => $page,
      'authenticated' => isset($_SESSION['user'])
    ]);
  }

  public function newTask()
  {
    $fields = [];
    $errors = [];

    if ($_POST) {
      $fields = [
        'username' => htmlspecialchars($_POST['username']),
        'email' => htmlspecialchars($_POST['email']),
        'text' => htmlspecialchars($_POST['text'])
      ];
      $errors = $this->validate($fields, [
        'email' => ['type' => 'email'],
        'username' => ['type' => 'length', 'value' => 2]
      ]);

      if ($_FILES['picture']['name']) {
        try {
          $fields['picture'] = Image::upload($_FILES['picture']);
        } catch (\Exception $e) {
          $errors['picture'] = $e->getMessage();
        }
      }
      if (empty($errors)) {
        Task::create($fields);
        header('Location: /portfolio/ta/taskmanager/');
      }
    }

    View::render('Tasks/new', [
      'fields' => $fields,
      'errors' => $errors,
      'authenticated' => isset($_SESSION['user'])
    ]);
  }

  public function edit()
  {
    $fields = [];
    $errors = [];

    if ($_POST) {
      $fields = [
        'text' => htmlspecialchars($_POST['text']),
      ];

      $errors = $this->validate($fields);
      if (empty($errors)) {
        $fields['completed'] = isset($_POST['completed']) ? htmlspecialchars($_POST['completed']) : 0;
        Task::update($this->routeParams['id'], $fields);
        if ($_GET['rdr']) {
          header("Location: /portfolio/ta/taskmanager/?page={$_GET['rdr']}");
        } else {
          header('Location: /portfolio/ta/taskmanager/');
        }
      }
    }

    View::render('Tasks/edit', [
      'task' => Task::get($this->routeParams['id']),
      'fields' => $fields,
      'errors' => $errors,
      'authenticated' => isset($_SESSION['user'])
    ]);
  }

  public function delete()
  {
    $picture = Task::get($this->routeParams['id'], 'picture')['picture'];
    if ($picture) {
      unlink($picture);
    }
    Task::delete($this->routeParams['id']);
    header('Location: /portfolio/ta/taskmanager/');
  }

  public function preview()
  {
    if ($_POST) {
      $fields = [
        'username' => htmlspecialchars($_POST['username']),
        'email' => htmlspecialchars($_POST['email']),
        'text' => htmlspecialchars($_POST['text']),
        'picture' => htmlspecialchars($_FILES['picture']),
      ];
    }
    View::render('Tasks/preview', $fields);
  }
}
