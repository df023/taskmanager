<?php

namespace App\Controllers;

use Core\View;
use App\Models\User;

class Users extends \Core\Controller
{
  use \App\Helpers\FormValidate;

  public function login()
  {
    $fields = [];
    $errors = [];

    if ($_POST) {
      $fields['username'] = $_POST['username'];
      $fields['password'] = $_POST['password'];
      $errors = $this->validate($fields);
      if (empty($errors)) {
        $user = User::get($fields['username']);
        if (password_verify($fields['password'], $user['password'])) {
          $_SESSION['user'] = $fields['username'];
          header("Location: /portfolio/ta/taskmanager/");
        } else {
          $errors['invalidLogin'] = 'Invalid Login Credentials';
        }
      }
    }

    View::render('Users/login', [
      'fields' => $fields,
      'errors' => $errors,
      'authenticated' => isset($_SESSION['user'])
    ]);
  }

  public function logout()
  {
    unset($_SESSION['user']);
    session_destroy();
    header("Location: /portfolio/ta/taskmanager/");
  }

  public function create()
  {
    $fields = [
      'username' => $_GET['username'],
      'password' => $_GET['password']
    ];

    $errors = $this->validate($fields, [
      'username' => ['type' => 'length', 'value' => 4],
      'password' => ['type' => 'length', 'value' => 3]
    ]);

    if (empty($errors)) {
      User::create($username, $password);
      header('Location: /');
    }
  }
}
