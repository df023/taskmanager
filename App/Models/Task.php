<?php

namespace App\Models;

use PDO;

class Task extends \Core\Model
{
  public static function fetch($start, $perPage, $sort)
  {
    if ($sort === 'created_at') {
      $sortDirection = 'DESC';
    } else {
      $sortDirection = 'ASC';
    }

    $db = static::getDB();
    $stmt = $db->query("SELECT * FROM tasks ORDER BY $sort $sortDirection LIMIT $perPage OFFSET $start");
    return [
      $db->query('SELECT COUNT(id) FROM tasks')->fetch()[0],
      $stmt->fetchAll(PDO::FETCH_ASSOC)
    ];
  }

  public static function get($taskId, $fields = '*')
  {
    $db = static::getDB();
    $stmt = $db->query(
      "SELECT $fields FROM tasks WHERE id = '$taskId' ORDER BY created_at DESC"
    );

    return $stmt->fetch(PDO::FETCH_ASSOC);
  }


  public static function create($task)
  {
    extract($task);
    $db = static::getDB();
    $db->exec(
      "INSERT INTO tasks (username, email, text, picture) VALUES ('$username', '$email', '$text', '$picture')"
    );
  }

  public static function update($taskId, $fields)
  {
    extract($fields);
    $db = static::getDB();
    $stmt = $db->prepare("UPDATE tasks SET text = :text, completed = :completed WHERE id = '$taskId'");
    $stmt->bindParam(':text', $text, PDO::PARAM_LOB);
    $stmt->bindParam(':completed', $completed, PDO::PARAM_INT);
    $stmt->execute();
  }

  public static function delete($id)
  {
    $db = static::getDB();
    $db->exec("DELETE FROM tasks WHERE id = '$id'");
  }
}
