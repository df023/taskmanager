<?php

namespace App;

class Config
{
  const dbProd = [
    'localhost',
    'taskmanager',
    'root',
    '',
  ];
  const dbDev = [
    'localhost',
    'taskmanager',
    'root',
    '',
  ];

  const imgMaxWidth = 320;
  const imgMaxHeight = 240;
}
