<?php

namespace App\Helpers;

trait FormValidate
{
  public function validate($fields, $filters = null)
  {
    $errors = [];
    foreach ($fields as $field => $value) {
      if (empty($value)) {
        $errors[$field] = ucfirst($field) . ' can not be blank';
      }
    }

    if ($filters) {
      foreach ($filters as $field => $filter) {
        if (empty($errors[$field])) {
          switch ($filter['type']) {
            case 'email':
              if (filter_var($fields[$field], FILTER_VALIDATE_EMAIL) === false) {
                $errors['email'] = 'Please provide a valid Email';
              }
              break;
            case 'length':
              if (strlen($fields[$field]) < $filter['value']) {
                $errors[$field] = "$field should be {$filter['value']} characters long or more";
              }
              break;
          }
        }
      }
    }


    return $errors;
  }
}