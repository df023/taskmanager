<?php

namespace App\Helpers;

use App\Config;

class Image
{
  public static function upload($file)
  {
    $fileName = $file['name'];
    $tmpPath = $file['tmp_name'];
    $imagePath = "images/$fileName";
    if (file_exists($imagePath)) {
      $parts = explode('.', $fileName);
      $parts[0] .= '-' . rand(100, 999);
      $imagePath = 'images/' . implode('.', $parts);
    }
    $fileDimensions = getimagesize($tmpPath);
    list($width, $height) = $fileDimensions;
    $fileType = explode('/', strtolower($fileDimensions['mime']))[1];
    if (in_array($fileType, ['jpeg', 'png', 'gif'])) {
      $createFromFunc = 'imagecreatefrom' . $fileType;
      $imageFunc = 'image' . $fileType;

      $src = $createFromFunc($tmpPath);

      $newWidth = $width;
      $newHeight = $height;
      if ($width >= $height && $width > Config::imgMaxWidth) {
        $newWidth = Config::imgMaxWidth;
        $newHeight = ($height / $width) * $newWidth;
      } else if ($height > $width && $height > Config::imgMaxHeight) {
        $newHeight = Config::imgMaxHeight;
        $newWidth = ($width / $height) * $newHeight;
      }

      $tmpImage = imagecreatetruecolor($newWidth, $newHeight);
      imagecopyresampled($tmpImage, $src, 0, 0, 0, 0,
          $newWidth, $newHeight, $width, $height);
      $imageFunc($tmpImage, $imagePath);
      $fields['picture'] = $imagePath;

      imagedestroy($src);
      imagedestroy($tmpImage);
      return $imagePath;
    } else {
      throw new \Exception('Please provide a picture in available formats');
    }
  }
}