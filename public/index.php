<?php

// twig
require_once '../vendor/autoload.php';

// autoloader
spl_autoload_register(function ($class) {
  $root = dirname(__DIR__);
  $file = $root . '/' . str_replace('\\', '/', $class) . '.php';
  if (is_readable($file)) {
    require $file;
  }
});

session_start();

$route = new Core\Router();

$route->add('', ['controller' => 'Tasks', 'action' => 'index']);
$route->add('login', ['controller' => 'Users', 'action' => 'login']);
$route->add('logout', ['controller' => 'Users', 'action' => 'logout']);
$route->add('tasks', ['controller' => 'Tasks', 'action' => 'index']);
$route->add('tasks/new', ['controller' => 'Tasks', 'action' => 'newTask']);
$route->add('{controller}/{action}');
$route->add('{controller}/{id:\d+}/{action}');

$route->submit();
