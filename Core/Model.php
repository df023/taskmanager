<?php

namespace Core;

use PDO;
use App\Config;

abstract class Model
{
  protected static function getDB()
  {
    static $db = null;
    list($host, $dbName, $user, $password) = Config::dbProd;
    if ($db === null) {
      $dsn = 'mysql:host=' . $host . ';dbname=' . $dbName;
      $db = new PDO($dsn, $user, $password);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    return $db;
  }
}
