<?php

namespace Core;

class Router
{
  protected $routes = [];
  protected $params = [];

  public function add($route, $params = [])
  {
    $route = preg_replace('/\//', '\\/', $route);

    // replace {variables} with named groups and custom vars
    $route = preg_replace('/\{([a-z]+)\}/', '(?P<\1>[a-z-]+)', $route);
    $route = preg_replace('/\{([a-z]+):([^\}]+)\}/', '(?P<\1>\2)', $route);

    $route = '/^' . $route . '$/i';

    $this->routes[$route] = $params;
  }

  public function getRoutes()
  {
    return $this->routes;
  }

  public function match($url)
  {
    foreach ($this->routes as $route => $params) {
      if (preg_match($route, $url, $matches)) {
        foreach ($matches as $key => $match) {
          if (is_string($key)) {
            $params[$key] = $match;
          }
        }
        $this->params = $params;
        return true;
      }
    }
    return false;
  }

  public function getParams()
  {
    return $this->params;
  }

  public function submit()
  {
    $url = $this->removeQueryStringVariables($_SERVER['QUERY_STRING']);

    if ($this->match($url)) {
      $controller = $this->getNamespace() . $this->convertToPascalCase($this->params['controller']);

      if (class_exists($controller)) {
        $controllerObject = new $controller($this->params);

        $action = $this->convertToCamelCase($this->params['action']);
        if (is_callable([$controllerObject, $action])) {
          $controllerObject->$action();
        } else {
          throw new \Exception("Method $action (in controller $controller) not found");
        }
      } else {
        throw new \Exception("Controller class $controller not found");
      }
    } else {
      throw new \Exception("No route matched", 404);
    }
  }

  private function convertToPascalCase($string)
  {
    return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
  }

  private function convertToCamelCase($string)
  {
    return lcfirst($this->convertToPascalCase($string));
  }

  private function removeQueryStringVariables($url)
  {
    if ($url != '') {
      $parts = explode('&', $url, 2);

      if (strpos($parts[0], '=') === false) {
        $url = $parts[0];
      } else {
        $url = '';
      }
    }

    return $url;
  }

  private function getNamespace()
  {
    $namespace = 'App\\Controllers\\';

    if (array_key_exists('namespace', $this->params)) {
      $namespace .= $this->params['namespace'] . '\\';
    }

    return $namespace;
  }
}
